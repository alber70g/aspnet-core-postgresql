# ASP.NET + Angular 2

Wat gaan we maken:

**Todo app**

- Losse client in Angular 2
- Backend in ASP.NET Core
- Database met MySql

**Functionaliteit**

- Registreren
- Inloggen
- Todo's CRUD

## Voorbereiding
Installeren van:

- ASP.NET Core SDK
- Visual Studio Code
- Node.js
- npm install -g yo generator-aspnet
- Postgres server (en installeer ngpsql driver met stack builder)

## Scaffolding van ASP.NET applicatie

```sh
$ yo aspnet
```

Aanmaken WebAPI project

## Project.json debugger
```json
"buildOptions": {
    "debugType": "portable",
    ...
}
```

## Database instellingen
Blogpost: [Using PostgreSQL with ASP.NET Core](http://dotnetthoughts.net/using-postgresql-with-aspnet-core/)

Gebruik de volgende packages:
```json
...,
"dependencies": {
    ...,
    "Microsoft.EntityFrameworkCore": "1.0.0",
    "Microsoft.EntityFrameworkCore.Design": "1.0.0-preview2-final",
    "Microsoft.EntityFrameworkCore.Tools": "1.0.0-preview1-final",
    "Npgsql.EntityFrameworkCore.PostgreSQL": "1.0.0"
},
"tools": {
    "Microsoft.AspNetCore.Server.IISIntegration.Tools": "1.0.0-preview2-final",
    "Microsoft.EntityFrameworkCore.Tools": "1.0.0-preview2-final"
},
...
```

Connection String in de `appsettings.json`
```json
...,
"Data": {
    "Connectionstring": "User ID=postgres;Password=root;Host=localhost;Port=5432;Database=tododb;Pooling=true;"
}
```

Initial migration
```sh
$ dotnet ef migrations add InitialMigration
```

Update database met behulp van de gegenereerde migrations
```sh
$ dotnet ef database update
```

## Authenticatie

!! Volgorde van het toevoegen van `DbContext` in `Startup.cs` is belangrijk,
hij moet namelijk aanwezig zijn voordat je andere services ervan laat gebruik
maken (bijvoorbeeld de IdentityService)
