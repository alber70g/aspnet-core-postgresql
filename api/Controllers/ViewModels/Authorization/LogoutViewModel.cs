﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ViewModels.Authorization {
    public class LogoutViewModel {
        [BindNever]
        public string RequestId { get; set; }
    }
}
