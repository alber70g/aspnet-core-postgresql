using System;

namespace Backend.Controllers.ViewModels
{
    public class SubscriptionBaseModel
    {
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;

    }

    public class SubscriptionViewModel : SubscriptionBaseModel
    {
        public int Id { get; set; }
    }
}