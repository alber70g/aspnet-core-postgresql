using System;

namespace Backend.Controllers
{
    public class CustomerBaseModel
    {
        public string Name { get; set; }
        public string Emailaddress { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }

    public class CustomerViewModel : CustomerBaseModel
    {
        public int Id { get; set; }
        public string Orders { get { return $"/api/Customers/{Id}/Orders"; } }
    }
}