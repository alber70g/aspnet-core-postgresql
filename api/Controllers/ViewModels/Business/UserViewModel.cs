using System;

namespace Backend.Controllers.ViewModels
{
    public class UserBaseModel
    {
        public string Name { get; set; }
        public string Emailaddress { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;

    }

    public class UserViewModel : UserBaseModel
    {
        public int Id { get; set; }
    }
}