using Backend.Data;
using Backend.Models;
using Backend.Controllers.Abstract;

namespace Backend.Controllers
{
    public class CustomersController : GenericController<Customer, CustomerBaseModel, CustomerViewModel>
    {
        public CustomersController(ApplicationDbContext context) : base(context, context.Customers)
        { }
    }
}