using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using Backend.Data;
using Backend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Backend.Controllers.Abstract
{
    [Route("api/[controller]")]
    public abstract class GenericController<TBusiness, TBase, TView> : Controller
        where TBusiness : BaseModel, new()
        where TBase : class, new()
        where TView : class, new()
    {
        private ApplicationDbContext _context;
        private IQueryable<TBusiness> dbset;
        private IQueryable<TBusiness> _dbset
        {
            get
            {
                if (expression != null)
                {
                    return dbset.Where(expression.Compile()(this.HttpContext)).AsQueryable();
                }
                return dbset;
            }
            set { dbset = value; }
        }
        public Expression<Func<HttpContext, Func<TBusiness, bool>>> expression;
        private IMapper _mapper;

        public GenericController(ApplicationDbContext context, DbSet<TBusiness> entity)
        {
            _context = context;
            _dbset = entity;

            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TBusiness, TView>();
                cfg.CreateMap<TBase, TBusiness>();
                cfg.ForAllMaps((tm, me) => me.ForAllMembers(option => option.Condition((source, destination, sourceMember) => sourceMember != null)));
            })
            .CreateMapper();
        }

        [Route("")]
        [HttpGet]
        public IEnumerable<TView> Get()
        {
            return _mapper.Map<IEnumerable<TView>>(_dbset);
        }

        [Route("{id:int}")]
        [HttpGet]
        [Authorize]
        public TView Get(int id)
        {
            return _mapper.Map<TView>(_dbset.First(e => e.Id == id));
        }

        [Route("{id:int}")]
        [HttpPut]
        public IActionResult Put(int id, [FromBody] TBase entityViewModel)
        {
            var entity = _dbset.First(e => e.Id == id);
            if (entity != null)
            {
                _mapper.Map<TBase, TBusiness>(entityViewModel, entity);

                _context.SaveChanges();

                return Ok(_mapper.Map<TView>(entity));
            }
            return NotFound();
        }

        [Route("")]
        [HttpPost]
        public TView Post([FromBody] TBase entityViewModel)
        {
            var entity = (_dbset as DbSet<TBusiness>).Add(_mapper.Map<TBusiness>(entityViewModel));
            _context.SaveChanges();

            return _mapper.Map<TView>(entity.Entity);
        }

        [Route("{id:int}")]
        [HttpDelete]
        public void Delete(int id)
        {
            var entity = _dbset.First(e => e.Id == id);
            (_dbset as DbSet<TBusiness>).Remove(entity);
            _context.SaveChanges();
        }
    }
}