using Backend.Controllers.Abstract;
using Backend.Controllers.ViewModels;
using Backend.Data;
using Backend.Models;

namespace Backend.Controllers
{
    public class RecordingsController : GenericController<Recording, RecordingBaseModel, RecordingViewModel>
    {
        public RecordingsController(ApplicationDbContext context) : base(context, context.Recordings)
        { }
    }
}