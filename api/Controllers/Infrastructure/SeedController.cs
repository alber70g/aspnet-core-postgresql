using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Data;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers.Infrastructure
{
    [Route("api/[controller]")]
    public class SeedController : Controller
    {
        private ApplicationDbContext _context;

        public SeedController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            var yvonne = new Customer
            {
                Name = "Yvonne van der Tas",
                Emailaddress = "blaat@blaat.nl",
                Orders = new List<Order>{
                    new Order {
                        Payment = new Payment {
                            FreeOfCharge = true,
                            IsPayed = false,
                            Cost = 0
                        },
                        DateCreated = DateTime.Now.AddDays(-10)
                    },
                    new Order {
                        Payment = new Payment {
                            FreeOfCharge = false,
                            IsPayed = true,
                            PayDate = DateTime.Now.AddDays(-9),
                            Cost = 2
                        },
                        DateCreated = DateTime.Now.AddDays(-18)
                    }
                }
            };

            _context.Customers.Add(yvonne);

            _context.SaveChanges();

            return Ok();
        }
    }
}
