using System.Collections.Generic;

namespace Backend.Models
{
    public class Order : BaseModel
    {
        public Order()
        {
            OrderRecordings = new List<OrderRecording>();
        }

        public Payment Payment { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public virtual List<OrderRecording> OrderRecordings { get; set; }

    }
}