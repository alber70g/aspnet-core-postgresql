using System;

namespace Backend.Models
{
    public class Payment : BaseModel
    {
        public bool FreeOfCharge { get; set; }
        public bool IsPayed { get; set; }
        public DateTime? PayDate { get; set; }
        public double Cost { get; set; }
    }
}