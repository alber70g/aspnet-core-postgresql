namespace Backend.Models
{
    public class OrderRecording
    {
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int RecordingId { get; set; }
        public Recording Recording { get; set; }
    }
}