using OpenIddict;

namespace Backend.Models
{
    public class ApplicationUser : OpenIddictUser
    {
        public string AuthenticationProvider { get; set; }
    }
}
