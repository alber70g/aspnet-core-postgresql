using System;

namespace Backend.Models
{
    public class Subscription : Order
    {
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}