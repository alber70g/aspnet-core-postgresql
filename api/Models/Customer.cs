using System.Collections.Generic;

namespace Backend.Models
{
    public class Customer : BaseModel
    {
        public Customer()
        {
            Orders = new List<Order>();
        }

        public string Name { get; set; }
        public string Emailaddress { get; set; }
        public virtual List<Order> Orders { get; set; }
    }
}